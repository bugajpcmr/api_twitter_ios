//
//  PostCell.swift
//  api_twitter
//
//  Created by Bartosz Bugajski on 09/09/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class PostCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .blue
        label.font = .systemFont(ofSize: 18)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 16)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.textColor = .black
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    let breakView: UIView = {
        let br = UIView()
        br.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        return br
    }()
    
    func setupViews(){
        addSubview(usernameLabel)
        addSubview(dateLabel)
        addSubview(descriptionLabel)
        addSubview(breakView)
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: usernameLabel)
        addConstraintsWithFormat(format: "H:[v0]-16-|", views: dateLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]", views: dateLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: descriptionLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]-16-[v1]", views: usernameLabel, descriptionLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: breakView)
        addConstraintsWithFormat(format: "V:[v0(1)]|", views: breakView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
