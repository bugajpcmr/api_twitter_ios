//
//  LoadPostCell.swift
//  api_twitter
//
//  Created by Bartosz Bugajski on 10/09/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit

class LoadPostCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let showPostButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 22)
        button.setTitle("POKAŻ OBIEKTY",for: .normal)
        return button
    }()
    
    let idLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj Id obiektu"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    func setupViews(){
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
