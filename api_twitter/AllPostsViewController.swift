//
//  AllPostsViewController.swift
//  api_twitter
//
//  Created by Bartosz Bugajski on 13/09/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class AllPostsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var allPostsArray = [SinglePost]()
    let postcellId = "postcell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ObjectManager.getObjects()
        loadDataFromAPI()
        setupCollectionView()
        registerCells()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Twitter"
        let btn = UIBarButtonItem(title: "Wyloguj", style: .plain, target: self, action: #selector(backButtonClicked))
        //        self.navigationController?.navigationBar.topItem?.backBarButtonItem = btn
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = btn
        
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    @objc func backButtonClicked(){
        print("usuwam obiekt")
        ObjectManager.deleteItem(username: appLogin)
        appLogin = ""
        appPassword = ""
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(PostCell.self, forCellWithReuseIdentifier: postcellId)
    }
    
    private func loadDataFromAPI()
    {
        let url = "https://twitter.cyrek.xyz/api/all-posts"
        Alamofire.request(url, method: .post).responseJSON { response in
            if let postJSON = response.result.value {
                
                let postObject:Dictionary = postJSON as! Dictionary<String, Any>
                
                let idDictionaryObject:Dictionary = postObject["posts"] as! Dictionary<String, Any>
                let textObject:String = idDictionaryObject["text"] as! String
                let userObject:String = idDictionaryObject["user"] as! String
                let timeObject:String = idDictionaryObject["time"] as! String
                let idObject:Int = idDictionaryObject["id"] as! Int
                print(idDictionaryObject)
                self.allPostsArray.append(SinglePost(id: idObject, user: userObject, time: timeObject, text: textObject))
                
                self.collectionView.reloadData()
            }
        }
    }
    
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postcellId, for: indexPath) as! PostCell
            
            cell.dateLabel.text = allPostsArray[indexPath.row].time
            cell.usernameLabel.text = allPostsArray[indexPath.row].user
            cell.descriptionLabel.text = allPostsArray[indexPath.row].text
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return allPostsArray.count
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            let height = Float(allPostsArray[indexPath.row].text.estimatedFrameHeight(textWidth: (view.frame.width) - 32, textSize: 15, fontWeight: .regular, lineSpacing: 5)) + 56.0
            
            return CGSize(width: view.frame.width, height: CGFloat(height))
        default:
            return CGSize(width: view.frame.width, height: 120)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

