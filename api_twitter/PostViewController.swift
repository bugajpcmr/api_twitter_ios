//
//  PostViewController.swift
//  api_twitter
//
//  Created by Bartosz Bugajski on 09/09/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

struct SinglePost: Decodable {
    let id: Int
    let user: String
    let time: String
    let text: String
}


class PostViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let postcellId = "postcell"
    var postArray = [SinglePost]()
    var postId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ObjectManager.getObjects()
        addButtonView()
        setupCollectionView()
        registerCells()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Twitter"
        let btn = UIBarButtonItem(title: "Wyloguj", style: .plain, target: self, action: #selector(backButtonClicked))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = btn
        
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    @objc func backButtonClicked(){
        print("usuwam obiekt")
        ObjectManager.deleteItem(username: appLogin)
        appLogin = ""
        appPassword = ""
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(PostCell.self, forCellWithReuseIdentifier: postcellId)
    }
    
    private func addButtonView(){
        let v = UIView()
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOpacity = 1
        v.layer.shadowOffset = .zero
        v.layer.shadowRadius = 2
        v.layer.shadowPath = UIBezierPath(rect: v.bounds).cgPath
        v.backgroundColor = .black
        view.addSubview(v)
        v.addSubview(showPostButton)
        v.addSubview(idLabel)
        
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
        v.addConstraintsWithFormat(format: "H:|[v0(\(view.frame.width/2))]-[v1(\(view.frame.width/2))]|", views: idLabel ,showPostButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: showPostButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: idLabel)
        v.layer.addShadow()
        
        showPostButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    let idLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "PODAJ ID POSTA"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!])
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    let showPostButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 20)
        button.setTitle("POKAŻ POST",for: .normal)
        return button
    }()
    
    @objc func buttonClicked(sender : UIButton){
        postId = Int(idLabel.text!)
        loadDataFromAPI()
        print("click")
    }
    
    private func loadDataFromAPI()
    {
        let url = "https://twitter.cyrek.xyz/api/user"
        let parameters: Parameters = ["id": postId!]
//                Alamofire.request("https://twitter.cyrek.xyz/micro-post/\(postId ?? 0)").responseJSON { response in
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            print(response)
            if let postJSON = response.result.value {
                let postObject:Dictionary = postJSON as! Dictionary<String, Any>
                let textObject:String = postObject["text"] as! String
                let userObject:String = postObject["user"] as! String
                let timeObject:String = postObject["time"] as! String
                let idObject:Int = postObject["id"] as! Int

                self.postArray.append(SinglePost(id: idObject, user: userObject, time: timeObject, text: textObject))
                self.collectionView.reloadData()
            }
        }
//        let jsonUrlString = "https://twitter.cyrek.xyz/micro-post/12"
//        guard let url = URL(string: jsonUrlString) else { return }
//
//        URLSession.shared.dataTask(with: url) { (data, response, err) in
//
//            guard let data = data else { return }
//
//            do {
//                let object = try JSONDecoder().decode(SinglePost.self, from: data)
//                print(object.text)
//                self.postArray.append(SinglePost(id: object.id, user: object.user, time: object.time, text: object.text))
//                self.collectionView.reloadData()
//
//            } catch let jsonErr{
//                print("Error serializing json!", jsonErr)
//            }
//
//            //let dataAsString = String(data: data, encoding: .utf8)
//            //print(dataAsString ?? "blabla")
//
//        }.resume()
    }
    
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postcellId, for: indexPath) as! PostCell
            
            cell.dateLabel.text = postArray[indexPath.row].time
            cell.usernameLabel.text = postArray[indexPath.row].user
            cell.descriptionLabel.text = postArray[indexPath.row].text
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return postArray.count
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            let height = Float(postArray[indexPath.row].text.estimatedFrameHeight(textWidth: (view.frame.width) - 32, textSize: 15, fontWeight: .regular, lineSpacing: 5)) + 56.0
            return CGSize(width: view.frame.width, height: CGFloat(height))
        default:
            return CGSize(width: view.frame.width, height: 120)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
