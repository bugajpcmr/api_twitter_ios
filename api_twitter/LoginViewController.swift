//
//  LoginViewController.swift
//  api_twitter
//
//  Created by Bartosz Bugajski on 10/09/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

var appLogin = "username"
var appPassword = "password"

class LoginViewController: UIViewController {
    
    var isUserIdentified: Bool?
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        addButtonView()
        ObjectManager.getObjects()
        if (!ObjectsArray.isEmpty)
        {
            appLogin = ObjectsArray[0].username
            appPassword = ObjectsArray[0].password
            
            loadingLogin()
            
        }
        view.backgroundColor = .white
        navigationItem.title = "Zaloguj się"
    }
    
    private func loadDataFromAPI()
    {
        let url = "https://twitter.cyrek.xyz/api/login" // This will be your link
        let parameters: Parameters = ["username": appLogin, "passwd": appPassword]      //This will be your parameter
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            if let postJSON = response.result.value {
                self.isUserIdentified = postJSON as? Bool
                print(self.isUserIdentified!)
            }
            self.loginButton.isEnabled = true
            self.loadingLabel.text = ""
            self.activityIndicator.removeFromSuperview()
            print("wczytuje dane")
            self.logInUser()
        }
        
//        Alamofire.request("https://twitter.cyrek.xyz/api/login?username=\(appLogin)&passwd=\(appPassword)").responseJSON { response in
    }
    
    private func showAlertWrongLogin(){
        let alert = UIAlertController(title: "Błąd", message: "Podano nieprawidłowe dane logowania", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            @unknown default:
                print("error")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func setupViews(){
        view.addSubview(loginLabel)
        view.addSubview(passwordLabel)
        view.addSubview(loadingLabel)
        
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: loginLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: passwordLabel)
        
        passwordLabel.addSubview(showPasswordButton)
        passwordLabel.addConstraintsWithFormat(format: "H:[v0]-16-|", views: showPasswordButton)
        passwordLabel.addConstraintsWithFormat(format: "V:|[v0]|", views: showPasswordButton)
        showPasswordButton.addTarget(self, action: #selector(showPasswordButtonClicked), for: .touchUpInside)
        
        showPasswordButton.setImage(UIImage(named: "showPassword_icon"), for: .normal)
        view.addConstraintsWithFormat(format: "H:|-60-[v0]-60-|", views: loadingLabel)
        
        view.addConstraintsWithFormat(format: "V:|-120-[v0(40)]-20-[v1(40)]-35-[v2(40)]", views: loginLabel, passwordLabel, loadingLabel)
    }
    
    //MARK: - Button footer
    private func addButtonView(){
        let v = UIView()
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOpacity = 1
        v.layer.shadowOffset = .zero
        v.layer.shadowRadius = 2
        v.layer.shadowPath = UIBezierPath(rect: v.bounds).cgPath
        v.backgroundColor = .white
        view.addSubview(v)
        v.addSubview(loginButton)
        
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
        v.addConstraintsWithFormat(format: "H:|[v0]|", views: loginButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: loginButton)
        
        loginButton.addTarget(self, action: #selector(buttonCheckValuesClicked), for: .touchUpInside)

    }
    
    private func addActivityIndicator() {
        view.addSubview(activityIndicator)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: activityIndicator)
        view.addConstraintsWithFormat(format: "V:|[v0]|", views: activityIndicator)
        activityIndicator.startAnimating()
    }
    
    private func loadingLogin() {
        loginButton.isEnabled = false
        addActivityIndicator()
        loadingLabel.text = "TRWA LOGOWANIE"
        
        loadDataFromAPI()
    }
    
    @objc func buttonCheckValuesClicked() {
        
        appLogin = loginLabel.text!
        appPassword = passwordLabel.text!
        
        loadingLogin()
    }
    
    private func logInUser(){
        if (isUserIdentified ?? false)
        {
            let userEntity = Object(username: appLogin, password: appPassword)
            ObjectManager.saveObject(object: userEntity)
            
            let desVC = PostViewController(collectionViewLayout: UICollectionViewFlowLayout())
            let navController = UINavigationController(rootViewController: desVC)
//            navigationController?.pushViewController(desVC, animated: true)
            present(navController, animated: true, completion: nil)
        }
        else{
            showAlertWrongLogin()
        }
    }
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.cornerRadius = 14
        button.titleLabel?.font = .boldSystemFont(ofSize: 22)
        button.setTitle("Zaloguj",for: .normal)
        return button
    }()
    
    let showPasswordButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    @objc func showPasswordButtonClicked() {
        passwordLabel.isSecureTextEntry.toggle()
        view.setNeedsLayout()
    }
    
    let loadingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    let loginLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        textBar.layer.addShadow()
        textBar.autocapitalizationType = .none
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj login"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    let passwordLabel: PaddedPasswordTextField = {
        let textBar = PaddedPasswordTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        textBar.layer.addShadow()
        textBar.autocapitalizationType = .none
        textBar.isSecureTextEntry = true
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj hasło"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
}
